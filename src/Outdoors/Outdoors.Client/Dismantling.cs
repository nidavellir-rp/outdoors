﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;

namespace Outdoors.Client
{
    internal class Dismantling : BaseScript
    {
        private WeaponHash _currentWeapon = WeaponHash.Unarmed;
        private Dictionary<int, int> _claimedAnimals = new Dictionary<int, int>();

        [EventHandler("onClientResourceStart")]
        public void OnClientResourceStart(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) return;

            Exports.Add("CanAnimalBeClaimed", new Func<int, bool>(entityId => !HasAnotherPlayerClaimedThisAnimal((Ped)Entity.FromHandle(entityId))));
            Exports.Add("DismantleAnimal", new Func<int, int?, Task<object>>(async (entityId, seconds) => await DismantleDeadAnimal((Ped)Entity.FromHandle(entityId), seconds ?? 5)));

            Exports.Add("NetworkCanAnimalBeClaimed", new Func<int, bool>(networkId => !HasAnotherPlayerClaimedThisAnimal((Ped)Entity.FromNetworkId(networkId))));
            Exports.Add("NetworkDismantleAnimal", new Func<int, int, Task<object>>(async (networkId, seconds) => await DismantleDeadAnimal((Ped)Entity.FromNetworkId(networkId), seconds)));

            Exports.Add("GetNearestDeadAnimal", new Func<int>(() => World.GetAllPeds()
                .Where(x => (!x?.IsHuman ?? false) && (x?.IsDead ?? false))
                .OrderBy(x => x?.Position.DistanceToSquared(Game.PlayerPed.Position))
                .FirstOrDefault()?.Handle ?? -1));

            Exports.Add("NetworkGetNearestDeadAnimal", new Func<int>(() => World.GetAllPeds()
                .Where(x => (!x?.IsHuman ?? false) && (x?.IsDead ?? false))
                .OrderBy(x => x?.Position.DistanceToSquared(Game.PlayerPed.Position))
                .FirstOrDefault()?.NetworkId ?? -1));
        }

        /// <summary>
        /// Approach and animate dismantling an animal
        /// </summary>
        /// <param name="animal"></param>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public async Task<object> DismantleDeadAnimal(Ped animal, int seconds = 5)
        {
            if (!(animal?.Exists() ?? false))
            {
                return new { Success = false, Code = "NoEntity", Message = "There is no entity with this id" };
            }

            if (animal?.IsPlayer ?? false)
            {
                // You shouldn't be dismantling players...
                return new { Success = false, Code = "EntityIsAPlayer", Message = "¨This method does not permit dismantling players" };
            }

            if (animal?.IsAlive ?? false)
            {
                return new { Success = false, Code = "EntityIsAlive", Message = "This entity is alive and won't like being dismantled" };
            }

            // Get the player ped
            Ped playerPed = Game.PlayerPed;

            // Check if the animal has been claimed by another individual
            if (HasAnotherPlayerClaimedThisAnimal(animal))
            {
                return new { Success = false, Code = "HasBeenClaimed", Message = "This entity has been claimed by another player for dismantling" };
            }

            // Inform the server, and other players, that we're claiming the animal
            TriggerServerEvent("outdoors:server:dismantling:claim", animal?.NetworkId);

            // Get the distance of the user to the animal
            float distanceToAnimal = animal.Position.DistanceToSquared(playerPed.Position);

            // Get the length of the animal
            float animalLength = (animal?.Model?.GetDimensions().Y ?? 3f) + 2f;

            // Ensure that we close the distance between the player and the animal before we can dismantle it
            if (animalLength < distanceToAnimal)
            {
                // Cancel all other tasks
                playerPed?.Task?.ClearAll();

                // Walk towards the animal
                playerPed?.Task?.GoTo(animal.Position);

                // Wait while we approach the animal
                // This can be cancelled by the player moving
                while (animal?.Position.DistanceToSquared(playerPed.Position) > animalLength)
                {
                    await Delay(0);

                    // Check if the player is trying to move
                    if (Utils.IsPlayerMoving())
                    {
                        // Stop walking to the animal and return control to the player
                        playerPed?.Task?.ClearAll();

                        // Inform the server, and other players, that we're releasing the animal claim
                        TriggerServerEvent("outdoors:server:dismantling:cancelled");

                        // Cancel dismantling this animal
                        return new { Success = false, Code = "Cancelled", Message = "The action was cancelled, because the player moved" };
                    }
                }
            }

            // Check if the player is in range of the animal
            if (animal?.Position.DistanceToSquared(playerPed.Position) <= animalLength)
            {
                // Check if the animation we're using for dismantling has been loaded
                if (!HasAnimDictLoaded("amb@medic@standing@tendtodead@idle_a"))
                {
                    // If the anim dict hasn't been loaded, request it now
                    RequestAnimDict("amb@medic@standing@tendtodead@idle_a");

                    // Maximum time we'll wait for the dictionary to load
                    var animationTimeout = DateTime.UtcNow.AddSeconds(5);

                    //  Wait for the dict to load
                    while (!HasAnimDictLoaded("amb@medic@standing@tendtodead@idle_a"))
                    {
                        await Delay(0);

                        // Timeout has been reached, break out of the loop
                        if (animationTimeout < DateTime.UtcNow) break;
                    }
                }

                // Disarm the player while dismantling
                DisarmPlayer();

                // Clear all ped tasks
                playerPed?.Task?.ClearAll();

                // Make the player turn towards the animal
                playerPed?.Task?.TurnTo(animal);

                // Give the ped a chance to turn
                await Delay(500);

                // Kneel and pick up the animal remains
                playerPed?.Task?.PlayAnimation("amb@medic@standing@tendtodead@idle_a", "idle_b", 3f, (int)TimeSpan.FromSeconds(seconds).TotalMilliseconds, AnimationFlags.Loop);

                // Give the animation a chance to start
                await Delay(500);

                // Wait for the animation to finish running
                while (IsEntityPlayingAnim(playerPed.Handle, "amb@medic@standing@tendtodead@idle_a", "idle_b", (int)AnimationFlags.Loop) && !HasAnotherPlayerClaimedThisAnimal(animal))
                {
                    await Delay(0);

                    // Check if the player is trying to move
                    if (Utils.IsPlayerMoving())
                    {
                        // Clear all ped tasks
                        playerPed?.Task?.ClearAll();

                        // Restore the player's weapon
                        RestoreWeapon();

                        // Inform the server, and other players, that we're releasing the animal claim
                        TriggerServerEvent("outdoors:server:dismantling:cancelled");

                        // Cancel dismantling this animal
                        return new { Success = false, Code = "Cancelled", Message = "The action was cancelled, because the player moved" };
                    }
                }

                // Clear all ped tasks
                playerPed?.Task?.ClearAll();

                // Ensure the animal hasn't been dismantled by someone else
                if ((animal?.Exists() ?? false) && !HasAnotherPlayerClaimedThisAnimal(animal))
                {
                    if (!animal?.IsPersistent ?? false)
                    {
                        animal.IsPersistent = true;
                    }

                    // Check if we need to get ownership fo the entity
                    if (!NetworkHasControlOfNetworkId(animal.NetworkId))
                    {
                        // Ensure we have control of the animal
                        NetworkRequestControlOfNetworkId(animal.NetworkId);

                        // Wait for the ownership to happen
                        while (!NetworkHasControlOfNetworkId(animal.NetworkId))
                        {
                            await Delay(0);
                        }
                    }

                    // Delete the animal
                    animal?.Delete();
                    animal?.MarkAsNoLongerNeeded();

                    // Restore the player's weapon
                    RestoreWeapon();

                    // Successfully dismantled the animal
                    return new { Success = true };
                }
            }

            // Check if the animal has been claimed by another individual
            if (HasAnotherPlayerClaimedThisAnimal(animal))
            {
                return new { Success = false, Code = "HasBeenClaimed", Message = "This entity has been claimed by another player for dismantling" };
            }

            // Inform the server, and other players, that we're releasing the animal claim
            TriggerServerEvent("outdoors:server:dismantling:cancelled");
            return new { Success = false, Code = "Cancelled", Message = "The animal could not be dismantled" };
        }

        /// <summary>
        /// Check if the animal has been claimed by another player
        /// </summary>
        /// <param name="animal"></param>
        /// <returns></returns>
        private bool HasAnotherPlayerClaimedThisAnimal(Ped animal) => _claimedAnimals?.Any(x => x.Value.Equals(animal?.NetworkId) && !x.Key.Equals(Game.PlayerPed?.NetworkId)) ?? false;

        /// <summary>
        /// Populate the list of claimed animals
        /// </summary>
        /// <param name="ownerNetworkId"></param>
        /// <param name="entityNetworkId"></param>
        [EventHandler("outdoors:dismantling:animal-claimed")]
        private void AnimalClaimed(int ownerNetworkId, int entityNetworkId)
        {
            // Release any previously held animals
            AnimalReleased(ownerNetworkId);

            // Claim this current animal
            _claimedAnimals?.Add(ownerNetworkId, entityNetworkId);
        }

        /// <summary>
        /// Remove released animals from the claimed list
        /// </summary>
        /// <param name="ownerNetworkId"></param>
        [EventHandler("outdoors:dismantling:animal-released")]
        private void AnimalReleased(int ownerNetworkId)
        {
            // Release any previously held animals
            _claimedAnimals?.Remove(ownerNetworkId);
        }

        /// <summary>
        ///
        /// </summary>
        private void DisarmPlayer()
        {
            // Check if the player is currently unarmed
            if (Game.PlayerPed?.Weapons?.Current?.Hash != WeaponHash.Unarmed)
            {
                // If the player has a weapon, get the hash of the weapon
                _currentWeapon = Game.PlayerPed?.Weapons?.Current.Hash ?? WeaponHash.Unarmed;

                // Change the player's weapon to unarmed
                Game.PlayerPed?.Weapons?.Select(WeaponHash.Unarmed, true);
            }
        }

        /// <summary>
        ///
        /// </summary>
        private void RestoreWeapon()
        {
            // Check if the player is currently unarmed, and if we have a stored wepaon hash
            if (Game.PlayerPed?.Weapons?.Current.Hash == WeaponHash.Unarmed && _currentWeapon != WeaponHash.Unarmed)
            {
                // Ensure that the player still has the weapon
                if (Game.PlayerPed.Weapons.HasWeapon(_currentWeapon))
                {
                    // Switch to the weapon they had stored
                    Game.PlayerPed?.Weapons?.Select(_currentWeapon, true);

                    // Reset stored weapon to default
                    _currentWeapon = WeaponHash.Unarmed;
                }
            }
        }
    }
}