﻿using CitizenFX.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;

namespace Outdoors.Client
{
    public static class Utils
    {
        /// <summary>
        /// A list of aquatic animals
        /// </summary>
        private static List<PedHash> _aquaticAnimals = new List<PedHash>() { PedHash.Dolphin, PedHash.Fish, PedHash.Humpback, PedHash.KillerWhale, PedHash.HammerShark, PedHash.TigerShark, PedHash.Stingray };

        /// <summary>
        /// Check if the player is pressing any of the movement keys
        /// </summary>
        /// <returns></returns>
        public static bool IsPlayerMoving()
        {
            // Key values can be found here: https://docs.fivem.net/docs/game-references/controls/
            return IsControlJustPressed(0, 32) // W
                || IsControlJustPressed(0, 33) // S
                || IsControlJustPressed(0, 34) // A
                || IsControlJustPressed(0, 35); // D
        }

        public static async Task<Vector3> FindGroundAtPositionAsync(Vector3 position)
        {
            // This will be used to get the return value from the groundz native.
            float groundZ = 850.0f;

            // Loop from 950 to 0 for the ground z coord, and take away 25 each time.
            for (float zz = 950.0f; zz >= 0f; zz -= 25f)
            {
                float z = zz;
                // The z coord is alternating between a very high number, and a very low one.
                // This way no matter the location, the actual ground z coord will always be found the fastest.
                // If going from top > bottom then it could take a long time to reach the bottom. And vice versa.
                // By alternating top/bottom each iteration, we minimize the time on average for ANY location on the map.
                if (zz % 2 != 0)
                {
                    z = 950f - zz;
                }

                RequestCollisionAtCoord(position.X, position.Y, z);

                // Bool used to determine if the groundz coord could be found.
                // Check for a ground z coord.
                bool found = GetGroundZFor_3dCoord(position.X, position.Y, z, ref groundZ, false);

                // If we found a ground z coord, end the search and return the new vector
                if (found)
                {
                    return new Vector3(position.X, position.Y, groundZ);
                }

                // Wait 10ms before trying the next location.
                await BaseScript.Delay(10);
            }

            return position;
        }

        public static Vector3 FindGroundAtPosition(Vector3 position)
        {
            // This will be used to get the return value from the groundz native.
            float groundZ = 850.0f;

            // Loop from 950 to 0 for the ground z coord, and take away 25 each time.
            for (float zz = 950.0f; zz >= 0f; zz -= 25f)
            {
                float z = zz;
                // The z coord is alternating between a very high number, and a very low one.
                // This way no matter the location, the actual ground z coord will always be found the fastest.
                // If going from top > bottom then it could take a long time to reach the bottom. And vice versa.
                // By alternating top/bottom each iteration, we minimize the time on average for ANY location on the map.
                if (zz % 2 != 0)
                {
                    z = 950f - zz;
                }

                RequestCollisionAtCoord(position.X, position.Y, z);

                // Bool used to determine if the groundz coord could be found.
                // Check for a ground z coord.
                bool found = GetGroundZFor_3dCoord(position.X, position.Y, z, ref groundZ, false);

                // If we found a ground z coord, end the search and return the new vector
                if (found)
                {
                    return new Vector3(position.X, position.Y, groundZ);
                }
            }

            return position;
        }

        public static bool IsAquaticAnimal(PedHash pedHash)
        {
            return _aquaticAnimals?.Any(x => x == pedHash) ?? false;
        }
    }
}
