﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using static Outdoors.Client.GameEnums;

namespace Outdoors.Client
{
    public class Hunting : BaseScript
    {
        private BehaviorOptions _behaviorOptions = new BehaviorOptions();
        private List<AnimalBase> _spawnedAnimals = new List<AnimalBase>();
        private List<PedHash> _predators = new List<PedHash>() { PedHash.MountainLion };
        private Random _rng;

        [EventHandler("onClientResourceStart")]
        private void OnResourceStart(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) return;

            _rng = new Random(DateTime.UtcNow.Millisecond);

            int.TryParse(GetResourceMetadata(resourceName, "player_startles_prey_at_distance", 0), out int pedStartlesPreyDistance);
            int.TryParse(GetResourceMetadata(resourceName, "predator_attacks_humans_at_distance", 0), out int predatorAttacksHumanPedDistance);


            if (pedStartlesPreyDistance > 0)
            {
                _behaviorOptions.PreyStartledDistance = pedStartlesPreyDistance;
            }

            if (predatorAttacksHumanPedDistance > 0)
            {
                _behaviorOptions.PredatorAttacksHumanPedDistance = predatorAttacksHumanPedDistance;
            }

            Exports.Add("SpawnDeer", new Action<Vector3>(SpawnDeerAsync));
            Exports.Add("SpawnBoar", new Action<Vector3>(SpawnBoarAsync));
            Exports.Add("SpawnRabbit", new Action<Vector3>(SpawnRabbitAsync));
            Exports.Add("SpawnMountainLion", new Action<Vector3>(SpawnMountainLionAsync));
            Exports.Add("SpawnTigerShark", new Action<Vector3>(SpawnTigerSharkAsync));
            Exports.Add("SpawnHammerheadShark", new Action<Vector3>(SpawnHammerheadSharkAsync));
            Exports.Add("SpawnDolphin", new Action<Vector3>(SpawnDolphinAsync));
            Exports.Add("SpawnKillerWhale", new Action<Vector3>(SpawnKillerWhaleAsync));
            Exports.Add("SpawnAnimal", new Func<string, Vector3, object>(SpawnAnimalWrapperAsync));
        }

        /// <summary>
        /// Spawn a deer for hunting
        /// </summary>
        /// <param name="location"></param>
        public async void SpawnDeerAsync(Vector3 location)
        {
            await SpawnAnimalAsync(PedHash.Deer, location);
        }

        /// <summary>
        /// Spawn a boar for hunting
        /// </summary>
        /// <param name="location"></param>
        public async void SpawnBoarAsync(Vector3 location)
        {
            await SpawnAnimalAsync(PedHash.Boar, location);
        }

        /// <summary>
        /// Spawn a rabbit for hunting
        /// </summary>
        /// <param name="location"></param>
        public async void SpawnRabbitAsync(Vector3 location)
        {
            await SpawnAnimalAsync(PedHash.Rabbit, location);
        }

        /// <summary>
        /// Spawn a mountain lion for hunting or be hunted
        /// </summary>
        /// <param name="location"></param>
        public async void SpawnMountainLionAsync(Vector3 location)
        {
            await SpawnAnimalAsync(PedHash.MountainLion, location);
        }

        /// <summary>
        /// Spawn a tiger shark for hunting or be hunted
        /// </summary>
        /// <param name="location"></param>
        public async void SpawnTigerSharkAsync(Vector3 location)
        {
            await SpawnAnimalAsync(PedHash.TigerShark, location);
        }

        /// <summary>
        /// Spawn a hammerhead shark for hunting or be hunted
        /// </summary>
        /// <param name="location"></param>
        public async void SpawnHammerheadSharkAsync(Vector3 location)
        {
            await SpawnAnimalAsync(PedHash.HammerShark, location);
        }

        /// <summary>
        /// Spawn a dolphin for hunting
        /// </summary>
        /// <param name="location"></param>
        public async void SpawnDolphinAsync(Vector3 location)
        {
            await SpawnAnimalAsync(PedHash.Dolphin, location);
        }

        /// <summary>
        /// Spawn a killer whale for hunting
        /// </summary>
        /// <param name="location"></param>
        public async void SpawnKillerWhaleAsync(Vector3 location)
        {
            await SpawnAnimalAsync(PedHash.KillerWhale, location);
        }

        /// <summary>
        /// Wrapper for spawning any type of ped with the behavior of an animal.
        /// </summary>
        /// <param name="pedType"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        public async Task<object> SpawnAnimalWrapperAsync(string pedType, Vector3 location)
        {
            // Try to spawn the given animal with a provided string
            if (Enum.TryParse(pedType, true, out PedHash hash))
            {
                bool success = await SpawnAnimalAsync(hash, location);
                return new { Success = success, Message = $"Spawned animal of type {hash}" };
            }

            return new { Success = false, Message = $"No ped matched model {pedType}" };
        }

        /// <summary>
        /// Spawn a specific animal
        /// </summary>
        /// <param name="hash"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        public async Task<bool> SpawnAnimalAsync(PedHash hash, Vector3 location)
        {
            // Work out the uint for the hash value
            uint modelHash = (uint)hash;

            // Check if the model is valid
            if (!IsModelValid(modelHash))
            {
                return false;
            }

            // Track if model was requested
            bool wasModelRequested = false;

            // Request model
            if (!HasModelLoaded(modelHash))
            {
                RequestModel(modelHash);
                wasModelRequested = true;
            }

            // Fetch the animation presets and request the dictionary
            AnimationPreset preset = AnimalBase.GetAnimation(hash);

            // Request the animation dictioary
            if (!preset?.HasLoaded ?? false)
            {
                RequestAnimDict(preset.Dictionary);
            }

            // Wait for the model & anim dict to load
            while (!HasModelLoaded(modelHash) || (!preset?.HasLoaded ?? false))
            {
                await Delay(0);
            }

            // Figure out a random heading
            float heading = (float)(360 * _rng.NextDouble());

            // Find safe placement for the ped
            Vector3 groundAtCoordinates = await Utils.FindGroundAtPositionAsync(location);

            // Check if the animal is aquatic
            if (Utils.IsAquaticAnimal(hash))
            {
                // Get the water height at the location we're spawning the animal
                float waterHeight = 0f;
                GetWaterHeightNoWaves(groundAtCoordinates.X, groundAtCoordinates.Y, groundAtCoordinates.Z, ref waterHeight);

                // If desired location is between ground and water height, use the original location
                if (groundAtCoordinates.Z < location.Z && location.Z < waterHeight)
                {
                    groundAtCoordinates.Z = location.Z;
                }
                else if (groundAtCoordinates.Z < waterHeight)
                {
                    // Random placement between ground and water level
                    float heightDifference = (waterHeight - groundAtCoordinates.Z) * (float)(_rng?.NextDouble() ?? 0.5f);

                    // Apply the new difference to the coordinates
                    groundAtCoordinates.Z += heightDifference;
                }
            }

            // Tell the server to spawn our prey
            TriggerServerEvent("outdoors:server:hunting:spawn-animal", (int)PedTypes.PED_TYPE_ANIMAL, modelHash, groundAtCoordinates, heading);

            // Unload the model from memory if it was requested
            if (wasModelRequested)
            {
                await Delay(5000);
                SetModelAsNoLongerNeeded(modelHash);
            }

            return true;
        }

        /// <summary>
        /// Create a spanwed prey object and store it in the list
        /// </summary>
        /// <param name="networkId"></param>
        /// <param name="modelHash"></param>
        /// <param name="ownerNetworkId"></param>
        [EventHandler("outdoors:hunting:spawned-animal")]
        public async void AnimalSpawned(int networkId, uint modelHash, int ownerNetworkId)
        {
            // Define a timeout
            DateTime timeout = DateTime.UtcNow.AddSeconds(5);

            // Cast the model hash to a PedHash
            PedHash pedHash = (PedHash)modelHash;

            // Wait for the animal to be spawned
            while (!NetworkDoesEntityExistWithNetworkId(networkId))
            {
                // Check if timeout has expired
                if (timeout < DateTime.UtcNow)
                {
                    Debug.WriteLine($"Failed to spawn animal of type {pedHash}");
                    return;
                }

                await Delay(0);
            }

            // Create a new instance of the animal
            if (_predators?.Any(x => x == pedHash) ?? false)
            {
                _spawnedAnimals?.Add(new Predator(networkId, modelHash, Game.PlayerPed?.NetworkId.Equals(ownerNetworkId) ?? false, _rng));
            }
            else if (pedHash == PedHash.TigerShark || pedHash == PedHash.HammerShark)
            {
                _spawnedAnimals?.Add(new Shark(networkId, modelHash, Game.PlayerPed?.NetworkId.Equals(ownerNetworkId) ?? false, _rng));
            }
            else
            {
                // Add spawned prey to the client's list
                _spawnedAnimals?.Add(new Prey(networkId, modelHash, Game.PlayerPed?.NetworkId.Equals(ownerNetworkId) ?? false, _rng));
            }
        }

        /// <summary>
        /// Update ownership of tracked creatures
        /// </summary>
        /// <param name="entityNetworkId"></param>
        /// <param name="ownerNetworkId"></param>
        /// <param name="modelHash"></param>
        [EventHandler("outdoors:hunting:update-animal-ownership")]
        public async void UpdateAnimalOwnership(int entityNetworkId, int ownerNetworkId, uint modelHash)
        {
            // Check if the creature is owned by me
            if (Game.PlayerPed?.NetworkId.Equals(ownerNetworkId) ?? false)
            {
                // Set a timeout for the loop
                DateTime timeout = DateTime.UtcNow.AddSeconds(5);

                // Wait for the entity to exist on the client and network
                while (!NetworkDoesEntityExistWithNetworkId(entityNetworkId))
                {
                    await Delay(0);

                    // Check for timeout
                    if (timeout < DateTime.UtcNow) break;
                }
            }

            if (NetworkDoesEntityExistWithNetworkId(entityNetworkId))
            {
                // Get the prey if it exists in the list
                AnimalBase animal = _spawnedAnimals?.FirstOrDefault(x => x?.Creature.Equals(Entity.FromNetworkId(entityNetworkId)) ?? false);

                // Ensure the animal exists
                if (animal != null)
                {
                    // Set ownership status
                    animal.IsOwner = Game.Player?.Handle.Equals(ownerNetworkId) ?? false;
                    return;
                }

                // Animal didn't exist, so we create a new instance of it
                AnimalSpawned(entityNetworkId, modelHash, Game.PlayerPed.NetworkId);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        [Tick]
        public async Task RunPerTickTasksAsync()
        {
            // Remove all prey that has died or no longer exists
            _spawnedAnimals?.RemoveAll(x => !PedExistsAndIsAlive(x?.Creature));

            // If we have any spawned creatures lets check in on them
            if (_spawnedAnimals.Count > 0)
            {
                // Get alive player peds
                Ped[] players = GetLivingPeds();

                foreach (AnimalBase animal in _spawnedAnimals?.Where(x => (x?.IsOwner ?? false) && (x?.Creature?.Exists() ?? false)))
                {
                    // Animate owned creatures
                    animal?.BringCreatureToLife(players, _behaviorOptions);
                }
            }

            // Run check every second
            await Delay(1000);
        }

        /// <summary>
        /// Get all living player peds
        /// </summary>
        /// <returns></returns>
        private Ped[] GetLivingPeds()
        {
            return World.GetAllPeds()?
                .Where(x => (x?.IsAlive ?? false))?
                .ToArray();
        }

        /// <summary>
        /// Checks if the ped exists, and if it's alive
        /// </summary>
        /// <param name="ped"></param>
        /// <returns></returns>
        private bool PedExistsAndIsAlive(Ped ped)
        {
            return (ped?.Exists() ?? false) && (ped?.IsAlive ?? false);
        }
    }

    public static class ExtensionMethods
    {
        /// <summary>
        /// Retrieve the nearest human that's on foot
        /// </summary>
        /// <param name="peds"></param>
        /// <param name="animal"></param>
        /// <returns></returns>
        public static Ped GetNearestHumanOnFoot(this Ped[] peds, Ped animal)
        {
            if (!(animal?.Exists() ?? false))
            {
                return default;
            }

            return peds?
                    .Where(x => (x?.IsHuman ?? false) && (!x?.IsInVehicle() ?? false))?
                    .OrderBy(x => x?.Position.DistanceToSquared(animal.Position))?
                    .FirstOrDefault();
        }

        /// <summary>
        /// Retrieve the closest player ped
        /// </summary>
        /// <returns></returns>
        public static Ped GetNearestPlayer(this Ped[] peds, Ped animal)
        {
            if (!(animal?.Exists() ?? false))
            {
                return default;
            }

            return peds?
                    .Where(x => x?.IsPlayer ?? false)?
                    .OrderBy(x => x?.Position.DistanceToSquared(animal.Position))?
                    .FirstOrDefault();
        }
    }
}