﻿using CitizenFX.Core;
using System;
using static CitizenFX.Core.Native.API;

namespace Outdoors.Client
{
    public class Predator : AnimalBase
    {
        private PredatorState State { get; set; }

        public Predator(int pedNetworkId, uint modelHash, bool isOwner, Random rng) : base(pedNetworkId, modelHash, isOwner, rng)
        {
            if (Creature?.Exists() ?? false)
            {
                // The predators here doesn't like water
                SetPedDiesInWater(Creature.Handle, true);
            }
        }

        private void SetPredatorState(PredatorState state)
        {
            State = state;
            NextChange = DateTime.UtcNow.AddSeconds(_rng.Next(15, 120));
        }

        /// <summary>
        /// Predators are kept as simple creatures, attack or wander
        /// </summary>
        /// <param name="peds"></param>
        /// <param name="options"></param>
        public override void BringCreatureToLife(Ped[] peds, BehaviorOptions options = null)
        {
            // I'm not the owner, I don't do animations for this prey
            if (!IsOwner) return;

            // Animal is in a state where it cannot change state
            if (!AnimalCanChangeAnimation) return;

            Ped nearestHuman = peds?.GetNearestHumanOnFoot(Creature);

            if (State == PredatorState.Unknown)
            {
                // Initialize wandering
                Wander();
            }
            else if (State != PredatorState.Attacking)
            {
                if (nearestHuman?.Position.DistanceToSquared(Creature.Position) < (options?.PredatorAttacksHumanPedDistance ?? new BehaviorOptions().PredatorAttacksHumanPedDistance))
                {
                    // Predator behavior state change
                    SetPredatorState(PredatorState.Attacking);
                    // Clear all current tasks
                    Creature?.Task?.ClearAll();
                    // Start fighting the nearest ped
                    Creature?.Task?.FightAgainst(nearestHuman);
                }
            }
            else if (NextChange < DateTime.UtcNow && (nearestHuman == null || !(Creature?.IsInCombat ?? false)))
            {
                Wander();
            }
        }

        private void Wander()
        {
            // Predator behavior state change
            SetPredatorState(PredatorState.Wandering);
            // Clear all current tasks
            Creature?.Task?.ClearAll();
            // Wander around the area
            Creature?.Task?.WanderAround();
        }

        private enum PredatorState
        {
            Unknown,
            Wandering,
            Attacking
        }
    }
}
