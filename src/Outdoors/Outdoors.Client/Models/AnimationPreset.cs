﻿using static CitizenFX.Core.Native.API;

namespace Outdoors.Client
{
    public class AnimationPreset
    {
        public string Dictionary { get; set; }
        public string Name { get; set; }
        public bool IsValid { get => !string.IsNullOrEmpty(Dictionary) && !string.IsNullOrEmpty(Name); }
        public bool HasLoaded
        {
            get
            {
                // If the dictionary isn't valid we just say it's loaded
                if (!IsValid) return true;

                // Check if the dictionary has loaded
                return HasAnimDictLoaded(Dictionary);
            }
        }

        public AnimationPreset()
        {
        }

        public AnimationPreset(string dictionary, string name)
        {
            Dictionary = dictionary;
            Name = name;
        }
    }
}