﻿using CitizenFX.Core;
using System;
using static CitizenFX.Core.Native.API;

namespace Outdoors.Client
{
    public class Prey : AnimalBase
    {
        public PreyState State { get; set; } = PreyState.Unknown;

        /// <summary>
        /// Initialize a new instance of the prey, it should have been created server-side
        /// </summary>
        /// <param name="pedNetworkId">Entity network ID</param>
        /// <param name="modelHash"></param>
        /// <param name="isOwner"></param>
        /// <param name="rng"></param>
        public Prey(int pedNetworkId, uint modelHash, bool isOwner, Random rng) : base(pedNetworkId, modelHash, isOwner, rng)
        {
            if (Creature?.Exists() ?? false)
            {
                if (Utils.IsAquaticAnimal((PedHash)modelHash))
                {
                    // Aquatic animals are fine with being in water
                    Creature.DiesInstantlyInWater = false;
                    Creature.DrownsInWater = false;
                }
                else
                {
                    // The prey here doesn't like water
                    Creature.DrownsInWater = true;
                }
            }
        }

        /// <summary>
        /// Update the state of the prey
        /// </summary>
        /// <param name="state"></param>
        public void PreyStateChange(PreyState state)
        {
            State = state;
            NextChange = DateTime.UtcNow.AddSeconds(_rng.Next(15, 120));
        }

        /// <summary>
        /// Animate the animal, so it won't just stand there like a rock
        /// </summary>
        /// <param name="peds"></param>
        /// <param name="options"></param>
        public override void BringCreatureToLife(Ped[] peds, BehaviorOptions options = null)
        {
            // I'm not the owner, I don't do animations for this prey
            if (!IsOwner) return;

            // Animal is in a state where it cannot change state
            if (!AnimalCanChangeAnimation) return;

            // The prey isn't currently fleeing
            if (State != PreyState.Fleeing)
            {
                // The creature isn't fleeing, check for nearest alive player ped
                Ped nearestPed = peds?.GetNearestPlayer(Creature);

                // If the nearest player is within the specified startling distance
                // cause the creature to flee
                if (nearestPed?.Position.DistanceToSquared(Creature.Position) < (options?.PreyStartledDistance ?? 500))
                {
                    // Prey behavior state change
                    PreyStateChange(PreyState.Fleeing);
                    // Clear all current tasks
                    Creature?.Task?.ClearAll();
                    // Flee from the nearest player ped
                    // The creature can flee for 15 - 90 seconds
                    int duration = (int)TimeSpan.FromSeconds(_rng.Next(15, 90)).TotalMilliseconds;
                    // Distance the creature will travel
                    TaskSmartFleePed(Creature.Handle, nearestPed.Handle, options?.PreyStartledDistance ?? 500, duration, true, true);
                }
                else if (NextChange < DateTime.UtcNow)
                {
                    // Is this prey willing to wander or does it prefer grazing?
                    bool willingToWander = Hash == PedHash.Deer ? _rng.NextDouble() < 0.3 : _rng.NextDouble() < 0.8;

                    // Get the animation dictionary and name
                    AnimationPreset preset = GetAnimation(Hash);

                    // Check if prey has been grazing, and is willing to wander
                    if (State == PreyState.Grazing && willingToWander || !(preset?.IsValid ?? false))
                    {
                        // Prey behavior state change
                        PreyStateChange(PreyState.Wandering);
                        // Clear all current tasks
                        Creature?.Task?.ClearAll();
                        // Wander around the area
                        Creature?.Task?.WanderAround();
                    }
                    else
                    {
                        // Check if the creature is already playing this animation
                        if (!IsEntityPlayingAnim(Creature.Handle, preset.Dictionary, preset.Name, 3))
                        {
                            // Prey behavior state change
                            PreyStateChange(PreyState.Grazing);
                            // Clear all current tasks
                            Creature?.Task?.ClearAll();
                            // Play the animation
                            Creature?.Task?.PlayAnimation(preset.Dictionary, preset.Name, 5f, -1, AnimationFlags.Loop);
                        }
                    }
                }
            }
            else if (!Creature?.IsFleeing ?? false)
            {
                // The prey has been fleeing, but has stopped and now starts to wander
                PreyStateChange(PreyState.Wandering);
                // Clear all current tasks
                Creature?.Task?.ClearAll();
                // Wander around the area
                Creature?.Task?.WanderAround();
            }
        }
    }

    public enum PreyState
    {
        Creating = -1,
        Unknown,
        Fleeing,
        Grazing,
        Wandering
    }
}