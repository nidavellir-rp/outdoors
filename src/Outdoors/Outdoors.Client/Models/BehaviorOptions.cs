﻿namespace Outdoors.Client
{
    public class BehaviorOptions
    {
        // Prey options
        public int PreyStartledDistance { get; set; } = 500;

        // Predator options
        public int PredatorAttacksHumanPedDistance { get; set; } = 2500;
    }
}
