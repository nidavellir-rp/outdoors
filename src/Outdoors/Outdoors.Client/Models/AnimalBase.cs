﻿using CitizenFX.Core;
using System;
using static CitizenFX.Core.Native.API;

namespace Outdoors.Client
{
    public class AnimalBase
    {
        protected Random _rng;

        public string CreatureId { get; } = Guid.NewGuid().ToString();
        public Ped Creature { get; set; }
        public PedHash Hash { get; set; }
        public Vector3 Location { get; }
        public DateTime NextChange { get; set; }
        public bool IsOwner { get; set; }

        public bool AnimalCanChangeAnimation
        {
            get
            {
                if (Creature?.IsDead ?? false)
                {
                    return false;
                }

                if (Creature?.IsFalling ?? false)
                {
                    return false;
                }

                if (Creature?.IsRagdoll ?? false)
                {
                    return false;
                }

                if (!Creature?.IsOnFoot ?? false)
                {
                    return false;
                }

                return true;
            }
        }

        public AnimalBase(int pedNetworkId, uint modelHash, bool isOwner, Random rng)
        {
            Creature = (Ped)Entity.FromNetworkId(pedNetworkId);
            Hash = (PedHash)modelHash;
            IsOwner = isOwner;

            if (Creature?.Exists() ?? false)
            {
                Location = Creature.Position;
                Creature.IsPersistent = true;
                Creature.IsPositionFrozen = false;
                Creature.IsInvincible = false;
                Creature.IsEnemy = true;
                Creature.CanBeTargetted = true;
                Creature.BlockPermanentEvents = false;

                if (Utils.IsAquaticAnimal(Hash))
                {
                    if (!(Creature?.IsInWater ?? false))
                    {
                        Creature?.Kill();
                    }
                }
            }

            _rng = rng ?? new Random(DateTime.UtcNow.Millisecond);
        }

        public virtual void BringCreatureToLife(Ped[] peds, BehaviorOptions options = null)
        {
        }

        public static AnimationPreset GetAnimation(PedHash hash)
        {
            switch (hash)
            {
                case PedHash.Deer:
                    return new AnimationPreset("creatures@deer@amb@world_deer_grazing@idle_a", "idle_b");

                case PedHash.Boar:
                    return new AnimationPreset("creatures@boar@amb@world_boar_grazing@idle_a", "idle_b");

                case PedHash.Rabbit:
                    return new AnimationPreset("creatures@rabbit@amb@world_rabbit_eating@idle_a", "idle_b");

                default:
                    return null;
            }
        }
    }
}