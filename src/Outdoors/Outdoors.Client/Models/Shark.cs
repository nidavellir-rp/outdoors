﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;

namespace Outdoors.Client
{
    public class Shark : AnimalBase
    {
        private SharkState State { get; set; }

        public Shark(int pedNetworkId, uint modelHash, bool isOwner, Random rng) : base(pedNetworkId, modelHash, isOwner, rng)
        {
            if (Creature?.Exists() ?? false)
            {
                Creature.DiesInstantlyInWater = false;
                Creature.DrownsInWater = false;
                Creature.RelationshipGroup = World.AddRelationshipGroup("SHARK");
                Creature?.RelationshipGroup?.SetRelationshipBetweenGroups(Game.PlayerPed.RelationshipGroup, Relationship.Hate, true);
            }
        }

        private void SetSharkState(SharkState state)
        {
            State = state;
            NextChange = DateTime.UtcNow.AddSeconds(_rng.Next(15, 120));
        }

        /// <summary>
        /// Predators are kept as simple creatures, attack or wander
        /// </summary>
        /// <param name="peds"></param>
        /// <param name="options"></param>
        public override void BringCreatureToLife(Ped[] peds, BehaviorOptions options = null)
        {
            // I'm not the owner, I don't do animations for this prey
            if (!IsOwner) return;

            // Animal is in a state where it cannot change state
            if (!AnimalCanChangeAnimation) return;

            Ped nearestHuman = peds?.GetNearestHumanOnFoot(Creature);

            // Check if the human is in the water
            bool humanIsInWater = (nearestHuman?.IsSwimming ?? false) || (nearestHuman?.IsSwimmingUnderWater ?? false);
            // Check if human is nearby
            bool isHumanNearby = nearestHuman?.Position.DistanceToSquared(Creature.Position) < (options?.PredatorAttacksHumanPedDistance ?? new BehaviorOptions().PredatorAttacksHumanPedDistance);

            if (!isHumanNearby && (NextChange < DateTime.UtcNow || State == SharkState.Unknown))
            {
                // Predator behavior state change
                SetSharkState(SharkState.Wandering);
                // Clear all current tasks
                Creature?.Task?.ClearAll();
                // Wander around the area
                Creature?.Task?.WanderAround();
            }
            else if (State != SharkState.Attacking && isHumanNearby)
            {
                if (humanIsInWater && isHumanNearby)
                {
                    // Predator behavior state change
                    SetSharkState(SharkState.Attacking);
                    // Clear all current tasks
                    Creature?.Task?.ClearAllImmediately();
                    // Start fighting the nearest ped
                    Creature?.Task?.FightAgainst(nearestHuman);
                }
            }
        }
    }

    public enum SharkState
    {
        Unknown,
        Wandering,
        Attacking
    }
}
