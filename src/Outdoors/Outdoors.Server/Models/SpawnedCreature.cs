﻿using CitizenFX.Core;
using System;
using static CitizenFX.Core.Native.API;


namespace Outdoors.Server
{
    public class SpawnedCreature
    {
        private Player _owner { get; set; }
        private DateTime _timeOfAbandonment { get; set; }

        public Ped Ped { get; }
        public uint ModelHash { get; }
        public Player Owner => _owner;
        public DateTime TimeToDeleteAsAbandoned => _timeOfAbandonment;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="modelHash"></param>
        public SpawnedCreature(int entityId, uint modelHash)
        {
            Ped = (Ped)Entity.FromHandle(entityId);
            ModelHash = modelHash;
        }

        public void UpdateOwnership()
        {
            // Check if the owner has changed
            if (!Ped?.Owner?.Equals(_owner) ?? false)
            {
                // Register what the last owner was for comparison
                Player lastOwner = _owner;

                // Owner has changed, update it
                _owner = Ped?.Owner;

                if (lastOwner != null)
                {
                    // Check if the current owner is server
                    if (Ped?.Owner?.Handle.Equals("-1") ?? false)
                    {
                        // Start a timer for abandoned creatures
                        _timeOfAbandonment = DateTime.UtcNow.AddMinutes(10);

                        // Check if the entity exists
                        if (DoesEntityExist(Ped.Handle))
                        {
                            // Check if the entity has been killed, or marked for deletion
                            if (GetEntityHealth(Ped.Handle) == 0)
                            {
                                // Remove the entity from the world
                                DeleteEntity(Ped.Handle);
                                return;
                            }
                        }

                        // If last owner was server, skip sending event to the previous owner
                        if (!lastOwner?.Handle.Equals("-1") ?? false)
                        {
                            // Update the ownership of the ped
                            lastOwner?.TriggerEvent("outdoors:hunting:update-animal-ownership", Ped?.NetworkId, Ped?.Owner?.Character?.NetworkId, ModelHash);
                        }
                        return;
                    }
                    else
                    {
                        // If last owner was server, skip sending event to the previous owner
                        if (!lastOwner?.Handle.Equals("-1") ?? false)
                        {
                            lastOwner?.TriggerEvent("outdoors:hunting:update-animal-ownership", Ped?.NetworkId, Ped?.Owner?.Character?.NetworkId, ModelHash);
                        }

                        // Trigger event for new owner to start animating the ped
                        Ped?.Owner?.TriggerEvent("outdoors:hunting:update-animal-ownership", Ped?.NetworkId, Ped?.Owner?.Character?.NetworkId, ModelHash);
                    }
                }
            }
            else if (Ped?.Owner?.Handle.Equals("-1") ?? false)
            {
                // Check if the ped has been abandoned
                if (TimeToDeleteAsAbandoned < DateTime.UtcNow)
                {
                    // Does the entity still exist
                    if (DoesEntityExist(Ped.Handle))
                    {
                        // Capture the ped network ID before it gets deleted
                        int? networkId = Ped?.NetworkId;

                        // Delete the entity
                        DeleteEntity(Ped.Handle);

                        // Trigger server event that an animal has been deleted
                        BaseScript.TriggerEvent("outdoors:hunting:animal-deleted", networkId);
                    }
                }
            }
        }
    }
}
