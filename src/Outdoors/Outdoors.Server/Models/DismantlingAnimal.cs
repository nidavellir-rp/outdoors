﻿using CitizenFX.Core;
using static CitizenFX.Core.Native.API;


namespace Outdoors.Server
{
    public class DismantlingAnimal
    {
        public Player Player { get; set; }
        public Ped Animal { get; set; }
        public bool AnimalExists => Animal != null && DoesEntityExist(Animal.Handle);

        public DismantlingAnimal() {}

        public DismantlingAnimal(Player player, int networkId)
        {
            Player = player;
            Animal = (Ped)Entity.FromNetworkId(networkId);
        }
    }
}
