﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;

namespace Outdoors.Server
{
    public class HuntingServer : BaseScript
    {
        private readonly List<SpawnedCreature> _creatures = new List<SpawnedCreature>();

        [EventHandler("onResourceStart")]
        private void OnResourceStart(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) return;

            Exports.Add("GetExistingPreyNetworkIds", new Func<int[]>(GetExistingPreyNetworkIds));
            Exports.Add("GetNumberOfSpawnedAnimals", new Func<int>(GetNumberOfSpawnedAnimals));
        }

        [EventHandler("onResourceStop")]
        private void OnResourceStop(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) return;

            // Remove entities created by the script
            foreach (Ped ped in _creatures?.Where(x => DoesEntityExist(x.Ped.Handle))?.Select(x => x?.Ped))
            {
                DeleteEntity(ped.Handle);
            }
        }

        /// <summary>
        /// Initiate spawning of the creature
        /// </summary>
        /// <param name="player"></param>
        /// <param name="pedType"></param>
        /// <param name="modelHash"></param>
        /// <param name="location"></param>
        /// <param name="heading"></param>
        [EventHandler("outdoors:server:hunting:spawn-animal")]
        private async void SpawnPrey([FromSource] Player player, int pedType, uint modelHash, Vector3 location, float heading)
        {
            // Spawn entity in the world
            int entity = CreatePed(pedType, modelHash, location.X, location.Y, location.Z, heading, true, true);

            // Set maximum wait time
            var maximumWaitTime = DateTime.UtcNow.AddSeconds(5);

            // Wait while the entity is created
            while (!DoesEntityExist(entity))
            {
                await Delay(0);

                // Maximum wait time exceeded, break the loop
                if (maximumWaitTime < DateTime.UtcNow) break;
            }

            if (DoesEntityExist(entity))
            {
                // Create the creature object
                var creature = new SpawnedCreature(entity, modelHash);

                // Add the creature to the server pool
                _creatures?.Add(creature);

                // Send the creature information to the client
                player.TriggerEvent("outdoors:hunting:spawned-animal", creature?.Ped?.NetworkId, modelHash, player?.Character?.NetworkId);
            }
            else
            {
                Debug.WriteLine($"{GetCurrentResourceName()} - Entity requested by {player?.Name} didn't exist within set time limit. Check if OneSync is enabled on the server. Model: {modelHash} | Location: {location}");
            }
        }

        /// <summary>
        /// Returns the list of network IDs belonging to creatures spawned
        /// </summary>
        /// <returns></returns>
        public int[] GetExistingPreyNetworkIds() => _creatures?.Where(x => DoesEntityExist(x?.Ped?.Handle ?? -1)).Select(x => x.Ped.NetworkId).ToArray();

        /// <summary>
        /// Returns the number of spawned animals still in the world
        /// </summary>
        /// <returns></returns>
        public int GetNumberOfSpawnedAnimals() => _creatures?.Count ?? 0;

        /// <summary>
        /// Cull removed animals and update ownership
        /// </summary>
        /// <returns></returns>
        [Tick]
        private async Task CullDeadAndDeletedCreatures()
        {
            // Remove all entities that don't exist
            _creatures?.RemoveAll(x => !DoesEntityExist(x.Ped.Handle));

            foreach (SpawnedCreature item in _creatures)
            {
                // Update owner for entities that exist
                item?.UpdateOwnership();
            }

            await Delay(500);
        }
    }
}
