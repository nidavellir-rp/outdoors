﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Outdoors.Server
{
    public class DismantlingServer : BaseScript
    {
        private List<DismantlingAnimal> _animalsBeingDismantled = new List<DismantlingAnimal>();

        [EventHandler("outdoors:server:dismantling:claim")]
        public void ClaimAnimalToDismantle([FromSource] Player player, int networkId)
        {
            if (!_animalsBeingDismantled?.Any(x => x?.Animal?.NetworkId.Equals(networkId) ?? false) ?? false)
            {
                TriggerClientEvent("outdoors:dismantling:animal-claimed", player?.Character?.NetworkId, networkId);
                _animalsBeingDismantled?.Add(new DismantlingAnimal(player, networkId));
            }
        }

        private void ReleaseAllAnimalsBelongingToPlayer(Player player)
        {
            // If there are any animals matching the player + network ID
            if (_animalsBeingDismantled?.Any(x => (x?.Player?.Equals(player) ?? false)) ?? false)
            {
                // Inform the connected clients of all claimed prey
                TriggerClientEvent("outdoors:dismantling:animal-released", player?.Character?.NetworkId);

                _animalsBeingDismantled?.RemoveAll(x => (x?.Player?.Equals(player) ?? false));
            }
        }

        [EventHandler("outdoors:server:dismantling:cancelled")]
        public void OnDismantlingCancelled([FromSource] Player player)
        {
            ReleaseAllAnimalsBelongingToPlayer(player);
        }

        [EventHandler("playerDropped")]
        public void OnPlayerDropped([FromSource] Player player)
        {
            ReleaseAllAnimalsBelongingToPlayer(player);
        }

        [EventHandler("playerConnecting")]
        public void InformOfAllAnimals([FromSource] Player player)
        {
            foreach (DismantlingAnimal item in _animalsBeingDismantled)
            {
                player?.TriggerEvent("outdoors:dismantling:animal-claimed", item?.Player?.Character?.NetworkId, item?.Animal?.NetworkId);
            }
        }

        [Tick]
        public async Task ClearDismantledAnimals()
        {
            int animalsRemoved = _animalsBeingDismantled?.RemoveAll(x => !x?.AnimalExists ?? false) ?? 0;

            await Delay(2500);
        }
    }
}
