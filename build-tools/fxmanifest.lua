fx_version 'adamant'
games {'gta5'}

author 'Nidavellir RP'
description 'This resource helps with creating creatures, such as deer and boars, for hunting purposes. The animals are spawned server-side, and remain even if the individual that spawned them disconnects from the server. The animals will swap ownership to another player if they are or get nearby when the previous owner leaves the scope.'
version 'custom-build'

-- Distance at which players will startle the prey
-- player_startles_prey_at_distance '500'

-- Distance at which predators will spot humans to attack
-- predator_attacks_humans_at_distance '2500'

client_script 'client.net.dll'
server_script 'server.net.dll'