# Outdoors

This resource helps with creating creatures, such as deer and boars, for hunting purposes.
The animals are spawned server-side, and remain even if the individual that spawned them disconnects from the server.

The animals will swap ownership to another player if they are or get nearby when the previous owner leaves the scope.

### This resource requires OneSync to be enabled on the server

## Configuration

```lua
// Distance at which players will startle the prey
player_startles_prey_at_distance '500'

// Distance at which predators will spot humans to attack
predator_attacks_humans_at_distance '2500'
```

## Hunting 

### Usage

```lua
RegisterCommand("deer", function(src,args) 
    local coords = GetOffsetFromEntityInWorldCoords(PlayerPedId(), 0, 5.0, 0)

    exports.outdoors:SpawnDeer(coords)
end)

RegisterCommand("lion", function(src,args) 
    local location = GetOffsetFromEntityInWorldCoords(PlayerPedId(), 0, 20.0, 0)

    exports.outdoors:SpawnMountainLion(location)
end)
```

#### SpawnDeer(Vector3 coordinates)

Spawns a single deer at the provided coordinates.

#### SpawnBoar(Vector3 coordinates)

Spawns a single boar at the provided coordinates.

#### SpawnRabbit(Vector3 coordinates)

Spawns a single rabbit at the provided coordinates.

#### SpawnDolphin(Vector3 coordinates)

Spawns a single dolphin at the provided coordinates. If the coordinates are above water, we will find a random spot in the water to spawn the animal. If there is no water, the animal will be killed upon spawning.

#### SpawnKillerWhale(Vector3 coordinates)

Spawns a single killer whale at the provided coordinates. If the coordinates are above water, we will find a random spot in the water to spawn the animal. If there is no water, the animal will be killed upon spawning.

### Predators

Predators will attack humans that are within a range of them, and not in a vehicle. They will wander around until they find a human to assault.

#### SpawnMountainLion(Vector3 coordinates)

Spawns a hostile mountain lion at the provided coordinates.

#### SpawnTigerShark(Vector3 coordinates)

Spawns a hostile tiger shark at the provided coordinates. If the coordinates are above water, we will find a random spot in the water to spawn the animal. If there is no water, the animal will be killed upon spawning.

#### SpawnHammerheadShark(Vector3 coordinates)

Spawns a hostile hammerhead shark at the provided coordinates. If the coordinates are above water, we will find a random spot in the water to spawn the animal. If there is no water, the animal will be killed upon spawning.

### Misc

#### SpawnAnimal(string pedHashKey, Vector3 coordinates)

Spawns a single animal or ped at the provided coordinates.

Aquatic animals will try and find a random spot in water at the coordinates provided. If no water is present, the animal will die upon being spawned.

Predator models will follow the behavior listed above, while all other peds will act like prey (including human models).

The [PedHash](https://github.com/citizenfx/fivem/blob/34069da52f788a6e21c7ae2e9fc6d84dda932abe/code/client/clrcore/External/PedHash.cs) version of the [model names](https://docs.fivem.net/docs/game-references/ped-models/#animals) should be used. I.e. "boar", "crow", "seagull", "killerwhale", or "fish".

The `pedHashKey` is not case sensitive.

## Dismantling

### Usage

```lua
RegisterCommand("chop", function(src,args) 
    local entity = exports.outdoors:GetNearestDeadAnimal()
    print("Got entity ID " .. entity)

    -- Do a check for distance so you won't be walking for miles

    if exports.outdoors:CanAnimalBeClaimed(entity) then
        local result = exports.outdoors:DismantleAnimal(entity, 2)

        if result.Success then
            -- Give some items for the animal
        end
    end
end)
```

#### GetNearestDeadAnimal()

Returns the nearest dead, non-human, animal in the world. You should do distance checks or provide your own entity IDs if you want to specify a particular entity.

#### CanAnimalBeClaimed(int entityId)

Checks if another player has claimed the entity for dismantling. Each player only holds one entity at a time.

#### DismantleAnimal(int entityId, int seconds)

**Only one player can dismantle an animal, once it's been successfully dismantled, it'll be deleted**

If the player ped is too far away from the entity, he'll start walking in the direction of it. He will then crouch down and start dismanting the prey.

The **seconds** parameter is optional, the function defaults to 5 seconds, but if you want a shorter duration you can provide it there. This dictates how long the player ped is crouching down over the prey.